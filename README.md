# TPFinal AppWeb 2018 - Buscar Autos #

Esta aplicacion permite realizar una busqueda de autos ingresando su Marca y Modelo. Para presentar la informacion, se realizara web scraping sobre disintas paginas como mercadolibre, carone, demotores y acara.

![alt text](img/cap1.png "Inicio")

**Realizando una busqueda:** en este caso se realiza una busqueda por vehiculos de marca Chevrolet y modelo Spin.

![alt text](img/cap2.png "buscando")

**Resultados**: se presentan los resultados de distintas paginas, mostrando el titulo, imagenes, precio y un link para navegar hacia el origen de la publicacion.

![alt text](img/cap3.png "resultados")

**Detalles:** al realizar click sobre cualquier item de la lista, presentamos un modal con caracteristicas del auto.

![alt text](img/cap4.png "acara")

**Precio oficial de Acara.org**: si es posible, para el item seleccionado sugerimos los precios oficiales de acara para versiones similares.

![alt text](img/cap5.png "Inicio")

## Requisitos ##
  - python 2.7
  - node 8

## Instalar y ejecutar ##

Server 

    $ pip install -r requirements.txt    
    $ python main.py

Ejecucion con gunicorn

    $ python2 /usr/local/bin/gunicorn -w 3 -b localhost:5000 main:app

Cliente

    $ cd client
    $ npm install
    $ npm start

Con Docker
    
    $ docker-compose build
    $ docker-compose up

Ingresar a 0.0.0.0:4200

## Sincronizacion con Acara ##

Para correr script y sincronizar con las opciones de aca:

    $ export FLASK_APP=$(pwd)/main.py
    $ flask scrap_opciones_acara

Con docker

    $ docker exec -ti <ID_container_server> bash
    $ export FLASK_APP=/src/main.py
    $ flask scra_opciones_acara


