import os
import unittest
import tempfile
from main import app
import json
from flask import request
class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        
    def test_index(self):
        response = self.app.get('/')
        data = json.loads(response.get_data())
        assert {"result":"index"} == data

    def test_opciones(self):
        response = self.app.get('/acara/opciones')
        data = json.loads(response.get_data())
        result = data.get('result')
        assert len(result.get('marcas')) > 0

    def test_precios_parametros_ok(self):
        q = {'marca':'178', 'modelo': '0006', 'version':'001'}
        response = self.app.get("/acara/precios", query_string=q)
        assert '404' not in response.status
        data = json.loads(response.get_data())
        result = data.get('result')
        assert type(result) == list
        
    def test_busqueda_mercadolibre(self):
        q = {'query':'auto'}
        response = self.app.get("/mercadolibre", query_string=q)
        assert '404' not in response.status
        data = json.loads(response.get_data())
        result = data.get('result')
        assert 'results' in result.keys()
    
    def test_busqueda_demotores(self):
        q = {'marca':'chevrolet', "modelo":"400"}
        response = self.app.get("/demotores", query_string=q)
        assert '404' not in response.status
        data = json.loads(response.get_data())
        assert data.get('result')

    def test_busqueda_carone(self):
        q = {'marca':'chevrolet', "modelo":""}
        response = self.app.get("/carone", query_string=q)
        assert '404' not in response.status
        data = json.loads(response.get_data())
        assert data.get('result')

if __name__ == '__main__':
    unittest.main()