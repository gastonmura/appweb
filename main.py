from flask import Flask, flash, jsonify, make_response, request, render_template, redirect
import json
from Scraping.Acara import Acara
from Scraping.Mercadolibre import Mercadolibre
from Scraping.Demotores import Demotores
from Scraping.Carone import Carone
from flask_pymongo import PyMongo
from flask_cors import CORS
import sys

app = Flask(__name__)
CORS(app)
app.config['JSON_AS_ASCII'] = False
app.config['FILE_ACARA'] = 'data/opciones.json'

@app.route('/')
def index():
    result = {"result": "index" }
    return jsonify(result)
    
@app.route('/acara/opciones')
def opciones():
    file = app.config['FILE_ACARA']
    try:
        data = json.load(open(file))["marcas"]
        data = [
        {"label": m.get('label'), 
        "modelos": m.get('modelos'),
        "codigo":m.get('codigo')
        } for m in data
        ] 
        result = {"ok": True, "result": data }
    except IOError as e:
        result = {"ok": False, "mensaje": "Sin datos almacenados de acara.org"}
    return jsonify(result)
        
@app.route('/acara')
def get_precios():
    marca = request.args.get('marca', '')
    modelo = request.args.get('modelo', '')
    version = request.args.get('version', '')
    acara = Acara()
    autos = acara.get_autos(marca, modelo, version)
    return jsonify( {"result": [a.serialize() for a in autos]})
    
@app.route('/mercadolibre')
def get_result():
    query = request.args.get('query', '')
    size = request.args.get('limit', 20)
    offset = request.args.get('offset', 0)
    m = Mercadolibre()
    try:
        busqueda = m.get_results(query = query, size = size, offset = offset)
        if busqueda:
            return jsonify( {"ok": True, "result":  busqueda.serialize() } )
        else:
            return jsonify( {"ok": False, "mensaje":  m.get_error() } )
            
    except ValueError as e:
        return jsonify( {"ok": False, "mensaje": str(e) } )



@app.route('/demotores')
def get_autos_demotores():
    marca = request.args.get('marca', '')
    modelo = request.args.get('modelo', '')
    query = request.args.get('query', '')
    size = request.args.get('limit', -1)
    offset = request.args.get('offset', 0)    
    
    demotores = Demotores()
    paginator = demotores.set_paginator(size, offset)
    if paginator.is_valid():        
        autos = demotores.get_autos(marca, modelo, query)
        return jsonify( {"ok": True, "result":  [a.serialize() for a in autos] } )
    else:
        return jsonify( {"ok":False, "mensaje": paginator.get_error() } )
        

@app.route('/carone')
def get_autos_carone():
    marca = request.args.get('marca', '')
    modelo = request.args.get('modelo', '')
    size = request.args.get('limit', -1)
    page = request.args.get('offset', 1)     

    carone = Carone()
    paginator = carone.set_paginator(size, page)
    if paginator.is_valid():    
        autos = carone.get_autos_usados(marca, modelo)
        autos.extend(carone.get_autos_0km(marca, modelo))        
        return jsonify( {"ok": True, "result": [a.serialize() for a in autos]})
    else:
        return jsonify( {"ok": False, "mensaje": paginator.get_error() } )
        
@app.cli.command()
def scrap_opciones_acara():
    import click
    import os
    import io
    from timeit import default_timer as timer

    file = app.config['FILE_ACARA']

    if os.path.exists(file):
        if not click.confirm('Desea sobreescribir el archivo?'):
            return
    
    click.echo('Scrapeando datos ...')
    acara = Acara()
    comienzo = timer()
    opciones = acara.get_opciones()
    try:
        with open(file, 'w') as outfile:
            json.dump(opciones, outfile, indent=4)
        fin = timer() - comienzo
        tam = outfile.__sizeof__()
        mensaje = "Datos scrapeados y almacenados. %d bytes en %d segundos."%(tam, fin)
        click.echo(mensaje)
    except IOError as e:
        click.echo('Error: ', str(e))
    
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding('utf-8')
    app.run(debug = True, host="0.0.0.0", port=5000)