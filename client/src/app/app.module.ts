import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { PreciosComponent } from './precios/precios.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { PrecioService } from './precio.service';
import { MaterializeModule } from 'angular2-materialize';
import { SpinnerComponent } from './spinner/spinner.component';
import { ModalDetalleComponent } from './modal-detalle/modal-detalle.component';
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
  declarations: [
    AppComponent,
    PreciosComponent,
    SpinnerComponent,
    ModalDetalleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MaterializeModule,
    InfiniteScrollModule
  ],
  providers: [PrecioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
