export class Auto {

  nombre: string;
  precio: string;
  imagenes: string[];
  link: string;
  pagina: string;
  moneda: string;
  detalle: string;
  fecha: string;
  match_autos : any[];

  constructor (nombre: string, precio: string, link: string, imagenes: string[], pagina: string, moneda: string, detalle: string, fecha: string, match_autos : any[]){
      this.nombre = nombre;
      this.precio = precio;
      this.imagenes = imagenes;
      this.link = link;
      this.pagina = pagina;
      this.moneda = moneda;
      this.detalle = detalle;
      this.fecha = fecha;
      this.match_autos = match_autos;
  }  
}
