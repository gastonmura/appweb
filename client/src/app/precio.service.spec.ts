import { TestBed, inject } from '@angular/core/testing';

import { PrecioService } from './precio.service';

describe('PrecioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrecioService]
    });
  });

  it('should be created', inject([PrecioService], (service: PrecioService) => {
    expect(service).toBeTruthy();
  }));
});
