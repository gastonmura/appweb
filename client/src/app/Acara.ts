/**
 * Clase usada para modelar la respuesta de scrapeo de Acara ante una marca modelo.
 */
export class Acara{
    autos :Auto[];
    
    constructor(){
        this.autos = [];
    }
    /**
     * Creamos objetos Auto.
     * @param json respuesta ante scrapeo por un determinada marca modelo.
     */
    load_from_json(json: any[]){
        this.autos = [];
        console.log(json);
        if (json.length == 0)
            return;
        var marca = json[0].marca;
        var modelo = json[0].modelo;
        for (let result of json){
            var auto = new Auto(marca, modelo, result.version, result.precios);
            this.autos.push(auto);
        }        
    }


    /**
     * Se realizara una comparacion entre nombre y la version de cada uno de los autos de acara.
     * Para retornar un listado de [Auto] ordenados con mas matcheos encontrados.
     * @param nombre string que se buscara en las versiones de los autos de Acara
     */
    get_precio_oficial(nombre : string ) : Auto[]{
        
        if (this.autos.length == 0){ return [];}
        
        var s = nombre.toLowerCase();
        var versiones = this.autos.map(a => a.version.toLowerCase());
        s = s.replace(this.autos[0].marca.toLowerCase(), "");
        s = s.replace(this.autos[0].modelo.toLowerCase(), "" );
        s = s.replace(/\s+/g, " ").trim();
        
        if ( this.isNullOrWhiteSpace(s)){return [];}
        
        var palabras = s.split(" ");
        var contadores : number[] = versiones.map(x =>  0);

        /* Por cada version, iniciamos un contador con cada palabra del nombre de auto que matchee */
        versiones.forEach( (version, index) => {
        for ( let palabra of palabras ){
            if ( version.includes(palabra)   ){
                contadores[index] = contadores[index] + 1;
            }
        }
        })
        /* Obtenemos los indices de todas las ocurrencias del valor maximo.  */
        var maximo = Math.max.apply(null, contadores);
        if (maximo == 0){ return [];}

        var index = -1;
        var indices : number[]=[];
        while( (index = contadores.indexOf(maximo, index + 1 )) != -1 ) { 
            indices.push(index);
        } 
        /* Obtenemos en indices[] los indices a las versiones con mas matcheos. */
        var result : Auto[] = [];
        for (let indice of indices){
            result.push(this.autos[indice]);
        }   
        return result;     
    }

    /**
     * Retorna true si la cadea es null, vacia, o con solo espacios \s
     * @param str 
     */
    isNullOrWhiteSpace(str: string): boolean {
        return (!str || str.length === 0 || /^\s*$/.test(str))
    }
}

/**
 * Clase usada para modelar cada uno de los resultados de Acara.
 * Cada auto tiene su marca modelo version y listado de precios oficiales de Acara.org.
 */
class Auto{

    marca : string;
    modelo : string;
    version : string;
    precios : any[];


    constructor(marca: string , modelo : string, version: string, precios: any[]){
        this.marca = marca;
        this.modelo = modelo;
        this.version = version;
        this.precios = precios;
    }
}