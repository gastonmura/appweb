import { Component, OnInit } from '@angular/core';
import { Auto } from '../Auto';
import { Acara } from '../Acara';

import { PrecioService } from '../precio.service' ;
import 'materialize-css';
import {MaterializeDirective,MaterializeAction} from "angular2-materialize";
import * as Materialize from 'angular2-materialize';
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import * as $ from 'jquery';

@Component({
  selector: 'app-precios',
  templateUrl: './precios.component.html',
  styleUrls: ['./precios.component.css']
})
export class PreciosComponent implements OnInit {
  
  autos : Auto[];
  acara : Acara = new Acara();
  marcas_acara : any[];
  marcaSelected:Number;
  modelos_acara : any[];
  modeloSelected: Number;
  resultado_acara;
  showSpinner: boolean = false;
  showResultados: boolean;
  total: number = 0;

  limit_general: number = 40; // solo s epuede de 20, 40, 60
  pagina_carone: number = 1; // se pide por pagina
  pagina_demotores: number = 0; // se pide el offset ej 0, 60, 120, 180, etc (de donde comineza a traer resultado) y la cantidad ej limit_general=60
  pagina_mercadolibre: number = 0; // se pide el offset ej 0, 60, 120, 180, etc (de donde comineza a traer resultado) y la cantidad ej limit_general=60

  scroll_carone: number = 1;
  scroll_demotores: number = 1;
  scroll_mercadolibre: number = 1;

  scroll_marca: any;  
  scroll_modelo: any;  
  
  constructor( private precioService: PrecioService) { }

  ngOnInit() {
    this.get_opciones();    

    // configuro el boton de "subir" cuando estamos muy abjo en el scroll de la web  
    var menu = $('.menu_ancla');
    var menu_offset = menu.offset();

    var h = $( window ).height();
    var w = $( window ).width();

    $('.subir_page').css("top",(h-60)+"px");
    $('.subir_page').css("left",(w-80)+"px");

    $('.subir_page').click(function(e){$('html, body').animate({scrollTop: '250px'}, 1000);});

    $(window).on('scroll', function() {
      if($(window).scrollTop() > menu_offset.top) {     
        $(".subir_page").fadeIn(300);
      } else {     
         $(".subir_page").fadeOut(300);
      }
    });

  }

  onModeloSelected(event):void{
    //var modelo = this.modelos_acara.find( x => x.codigo == this.modeloSelected );    
  }

  onMarcaSelected(event):void{
    var marca = this.marcas_acara.find( x => x.codigo == this.marcaSelected );    
    // controlo que la marca exista
    if( marca.codigo != 0 ){
      this.modelos_acara = marca.modelos;
      this.modeloSelected = this.modelos_acara[0].codigo;
    }else{
      this.modelos_acara = [];
      this.modeloSelected = marca.codigo;
      this.modeloSelected = null;
     }
  }

  /**
   * Consultamos a precioService para obtener las opciones de acara = marca -> modelos -> versiones,
   * seteandolas en marcas_acara bindeadas con la vista.
   */
  get_opciones(): void{
    this.precioService.get_opciones_acara().subscribe(
      result => {
        if (result.ok){
          this.marcas_acara = result['result'];
          this.marcaSelected = this.marcas_acara[0].codigo;
        }
        else{
          Materialize.toast(result.mensaje, 4000);
        }        
      }
    );
  }

  /**
   * Recibimos marca.codigo y modelo.codigo y consultamos precios a la API de ACARA
   * @param marca_codigo 
   * @param modelo_codigo 
   */
  precios_acara(marca_codigo : number, modelo_codigo : number): void{
    
    if ( marca_codigo == null || modelo_codigo == null ) {
      Materialize.toast('Error! Debe seleccionar una Marca y un Modelo para la busqueda!',4000);
      return;
    }

    this.total = 0;
    this.pagina_carone = 1; 
    this.pagina_demotores = 0; 
    this.pagina_mercadolibre = 0; 

    this.scroll_carone = 1;
    this.scroll_demotores = 1;
    this.scroll_mercadolibre = 1;

    this.showSpinner = true; 
    this.showResultados = false;

    var marca = this.marcas_acara.find( x => x.codigo == marca_codigo ); 
    var modelo = marca.modelos.find( x => x.codigo == modelo_codigo);
    
    this.scroll_marca =  marca;
    this.scroll_modelo = modelo;

    this.buscar_precios_acara(marca.codigo, modelo.codigo);
    this.buscar_precios(marca.label, modelo.label);
  }
  
  buscar_precios_acara(marca : number, modelo: number):void{
    this.precioService.get_precios_acara(marca, modelo)
      .subscribe(
        result => {
            this.acara.load_from_json(result.result);                       
        }
      );
  }
  /**
   * Recibimos nombre de marca modelo y realizamos busquedas a carone, demotores y mercadolibre.
   * En cada respuesta de la API se generan objetos Auto y se agregan al listado para ser mostrados.
   * @param marca 
   * @param modelo 
   */
  buscar_precios(marca : string, modelo : string) : void{
    
    if ( this.pagina_carone == 1 &&  this.pagina_demotores == 0  && this.pagina_mercadolibre == 0)
      this.autos = [];
    
    // carone
    if( this.scroll_carone != 0 ){
      this.precioService.carone(marca, modelo, this.limit_general, this.pagina_carone)
        .subscribe( result => {
          if (!result.ok){
            console.log("Error ", result.mensaje);
          }else{
            this.scroll_carone = result['result'].length;
            this.total += this.scroll_carone;
            for (let auto_json of result['result']){
              this.autos.push(new Auto(
                  auto_json['nombre'], 
                  auto_json['precio'], 
                  auto_json['link'],
                  ( new Array(auto_json['imagen']) ),
                  "Carone",
                  auto_json['moneda'],
                  auto_json['detalle'],
                  auto_json['fecha'],
                  this.acara.get_precio_oficial(auto_json['nombre'])));
            }
          }
          // cuando tenemos los primeros resultado comenzamos a mostrar
          this.showResultados = true;
          this.pagina_carone++;
        });
    }    
    // demotores
    if( this.scroll_demotores != 0 ){ 
      this.precioService.demotores(marca, modelo, this.limit_general, this.pagina_demotores)
        .subscribe( result => {
          if (!result.ok){
            console.log("Error ", result.mensaje);
          }
          else{
            this.scroll_demotores = result['result'].length;
            this.total += this.scroll_demotores;
            for (let auto_json of result['result']){  
              this.autos.push(new Auto(
                  auto_json['nombre'], 
                  auto_json['precio'], 
                  auto_json['link'],
                  auto_json['imagenes'],
                  "Demotores",
                  auto_json['moneda'],
                  auto_json['detalle'],
                  auto_json['fecha'],
                  this.acara.get_precio_oficial(auto_json['nombre'])));
            }
          }
          this.pagina_demotores += this.limit_general;
        });
    }
      // mercadolibre 
     if( this.scroll_mercadolibre != 0 ){ 
       this.precioService.mercadolibre(marca, modelo, this.limit_general, this.pagina_mercadolibre)
        .subscribe( result => {
          if (!result.ok){
            console.log("Error ", result.mensaje);
          }
          else{
            this.scroll_mercadolibre = result['result']['results'].length; 
            this.total += this.scroll_mercadolibre;       
            for (let auto_json of result['result']['results']){
              this.autos.push(new Auto(
                  auto_json['titulo'], 
                  auto_json['precio'], 
                  auto_json['link'],
                  auto_json['imagenes'],
                  "Mercadolibre",
                  auto_json['moneda'],
                  auto_json['detalle'],
                  auto_json['fecha'],
                  this.acara.get_precio_oficial(auto_json['titulo'])));
            }
          }
          this.showSpinner = false;
          this.pagina_mercadolibre += this.limit_general;
        });
      }  
  }


  onScroll() {
    if ( this.showSpinner == false ){
      this.showResultados = true;
      this.showSpinner = true;
      this.buscar_precios(this.scroll_marca.label, this.scroll_modelo.label);
      console.log("scrolled!!"); 
    }
      
  }

}
