import { Component, OnInit, EventEmitter} from '@angular/core';
import {MaterializeDirective,MaterializeAction} from "angular2-materialize";
import { Auto } from '../Auto';

@Component({
  selector: 'app-modal-detalle',
  templateUrl: './modal-detalle.component.html',
  styleUrls: ['./modal-detalle.component.css']
})
export class ModalDetalleComponent implements OnInit {

  auto: Auto;

  constructor() { }
  ngOnInit() {
  
  }
	
  modalActions1 = new EventEmitter<string|MaterializeAction>();
  params = []

  model1Params = [
    {
      dismissible: false,
      complete: function() { console.log('Closed'); }
    }
  ]

  openModal1(auto:Auto) {
  	this.auto = auto;
    this.modalActions1.emit({action:"modal",params:['open']});
    console.log(auto);
  }
  closeModal1() {
    this.modalActions1.emit({action:"modal",params:['close']});
}



}
