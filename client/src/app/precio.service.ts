import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import {Auto} from './Auto';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class PrecioService {

  private URLs = {
    "demotores": "http://localhost:5000/demotores",
    "carone": "http://localhost:5000/carone",
    "acara_precios": "http://localhost:5000/acara",
    "mercadolibre": "http://localhost:5000/mercadolibre",
    "acara_opciones": "http://localhost:5000/acara/opciones"
  };
  
  constructor( private http: HttpClient) { }

  demotores(marca : string, modelo : string, limit : number, offset : number): Observable<any>{
    const url = `${this.URLs['demotores']}?marca=${marca}&modelo=${modelo}&limit=${limit}&offset=${offset}`;
    return this.http.get(url, httpOptions);
  }

  carone(marca : string, modelo : string, limit : number, offset : number): Observable<any>{
    //var size = 15;
    var url = `${this.URLs['carone']}?marca=${marca}&modelo=${modelo}&limit=${limit}&offset=${offset}`;
    return this.http.get(url, httpOptions);
  }

  mercadolibre(marca : string, modelo : string, limit : number, offset : number): Observable<any>{
    var query = marca + " " + modelo;
    var  url = `${this.URLs['mercadolibre']}?query=${query}&limit=${limit}&offset=${offset}`;
    return this.http.get(url, httpOptions);
  }

  get_opciones_acara(): any{
    var url = this.URLs['acara_opciones'];
    return this.http.get(url);
  }

  get_precios_acara(marca : number, modelo : number): Observable<any>{
    var url = `${this.URLs['acara_precios']}?marca=${marca}&modelo=${modelo}`;
    return this.http.get(url, httpOptions);
  }

}
