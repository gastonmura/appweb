from Scraper import Scraper
from OpcionesScraper import OpcionesScraper
from PrecioScraper import PrecioScrapper

class Acara(Scraper):
    
    def __init__(self):
        self.opciones_scraper = OpcionesScraper()
        self.precios_scraper = PrecioScrapper()

    def get_opciones(self):
        """ Retornamos un python dict con las marcas modelos y versiones disponiles en acara """
        return self.opciones_scraper.get_opciones() 
    
    def get_autos(self, marca='', modelo='', version=''):
        """ Retornamos [Auto] cada uno con sus precios. """
        return self.precios_scraper.get_autos(marca, modelo, version)
