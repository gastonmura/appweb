import requests

class Scraper:
    sesion=None
    contador = None # contador de peticiones http como debug

    def __init__(self):
        self.sesion = requests.Session()
        self.contador = 0

    def go_to(self, url, data = None):
        """ Realizar peticion url, devolviendo (url, response_html)  """
        if data:
            response = self.sesion.post(url, data)
        else:
            response = self.sesion.get(url)
        self.contador = self.contador + 1
        encode = response.encoding if response.encoding else 'UTF-8'
        encode = 'UTF-8'
        return response.url, response.text.encode(encode)
