# -*- coding: utf-8 -*-
from Scraper import Scraper
import urllib
import json

class Mercadolibre(Scraper):

    FILTROS = {
        "category" : "MLA1743",  # autos y camionetas        
    }
    
    class Busqueda:
        query = None
        results = None
        def __init__(self , query = ""):
            self.query = query
            self.results = []

        def add_result(self, result):
            self.results.append(result)

        def get_total(self):
            return len(self.results)
        
        def __str__(self):    
            return "Busqueda ((query: %s), (%d resultados))" % (self.query, self.get_total())

        def serialize(self):
            return {
                "query": self.query,
                "results": [ p.serialize() for p in self.results]
            }
        
    class Result:
        
        titulo = None
        descripcion = None
        imagenes = None
        precio = None
        moneda = None
        link = None
        detalle =  None
        fecha = None
        
        def __init__(self):
            self.titulo = ""
            self.descripcion = ""
            self.imagenes = []
            self.precio = ""
            self.moneda = ""
            self.link = ""
            self.detalle = ""
            self.fecha = ""
        
        def add_imagen(self, path):
            self.imagenes.append(path)
            
        def __str__(self):
            return "Result (%s, %s %s, %s )"% (self.titulo, self.moneda, self.precio, self.link)
        
        def serialize(self):
            return {
                "titulo": self.titulo,
                "imagenes": self.imagenes,
                "precio": self.precio,
                "moneda": self.moneda,
                "link": self.link,
                "detalle": self.detalle,
                "fecha": self.fecha
            }
            
    Busqueda = Busqueda
    Result = Result
    error = ""
    url = "https://api.mercadolibre.com/sites/MLA/search"

    def get_error(self):
        """ Retornamos string error."""
        e = self.error
        self.error = ""
        return e

    def get_results(self, query, size = 10, offset = 0):
        """ Realizamos peticion http a la api de mercadolibre. Retornamos objeto Busqueda 
            Si query is empty lanzamos Exception.
            query!: query a buscar en mercadolibre, si es empty lanzamos ValueError.
            size: cantidad de resultados, por defecto 10, maximo permitido por mercadolibre es 50.
            offset : offset para paginacion, por defecto 0.
        """
        if query == None or  query == '':
            raise ValueError("Query no puede ser null")            
        
        data = {"q":query, "limit": size, "offset": offset, "category": self.FILTROS.get('category')}
        url_values = urllib.urlencode(data)
        full_url = self.url + '?' + url_values
        _, response_json =  self.go_to(url = full_url)
        response_json = json.loads(response_json)
        if response_json.get('error', "") != "":
            self.error = response_json.get('error') + " " + response_json.get('message')
            return None        
        return self.parse_results(response_json)
    
    def parse_results(self, response_json):
        """ Devolvemos objeto Busqueda con los resultados."""
        
        url_item = "https://api.mercadolibre.com/items/"
        busqueda = self.Busqueda(response_json.get('query'))
        
        for result in response_json.get("results"):
            _, response = self.go_to(url = url_item + result.get('id'))
            _, descripcion_fecha = self.go_to(url = url_item + result.get('id') + "/descriptions")
            desc_json = json.loads(descripcion_fecha)
            item = self.Result()
            for d in desc_json:
                item.detalle = d["plain_text"]
                item.fecha = d["created"]
            item_json = json.loads(response)
            for picture in item_json.get('pictures'):
                item.add_imagen(picture.get('url').strip())   
            item.titulo = result.get('title')
            item.precio = result.get('price')
            item.moneda = result.get('currency_id').strip()
            item.link = result.get('permalink').strip()
            busqueda.add_result(item)
        return busqueda