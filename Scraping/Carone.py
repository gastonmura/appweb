from Scraper import Scraper
from bs4 import BeautifulSoup 
import re
import time 
import datetime

class Carone(Scraper):

    url_carone = "http://www.carone.com.ar"
    url_usados = "http://www.carone.com.ar/autos-usados-"
    url_0km = "http://www.carone.com.ar/autos-0km-"

    def __init__(self):
        Scraper.__init__(self)
        self.paginator = self.Paginator(paginate = False)

    class Paginator:
        """ Clase que encapsula comportamiento de paginacion de la pagina Carone.
        Se envian parametros con method  POST:
          Ejemplos: 
          app = 100 y page = 1 ,se pide los primeros 100 resultados
          app = 200 y page = 2 se piden 200 resultados a partir del 200
        Esta clase resive size para mapear con app, y page para mapear con page.
        """
        def __init__(self, paginate = True):
            """ Iniciamos valores por defecto, size 100 y pagna 1. """
            self.size = 100
            self.page = 1
            self.paginate = paginate
        
        def configure(self, size, page):
            """ Configuracion del paginador de Carone. Si size es igual a -1. Se desactiva el paginador.
            Esto significa que la cantidad a pedir sera igual a la cantidad total de autos.
            Esto es asi para simplificar las peticiones http. Si size != -1 se configura el modo paginador.
            """
            if size == -1:
                self.paginate = False
            if page == -1:
                page = 1
            self.page = page
            self.size = size
            self.paginate = True

        def is_valid(self):
            """ Retorna valido si los parametros configurados en el paginador son correctos.
            Dichos parametros se validan con respecto a la paginacion de Carone.
            """
            try:
                self.size = int(self.size)
                self.page = int(self.page)
                return True
            except ValueError:
                self.error = "Size y Page deben ser valores enteros."
                return False   
        
        def get_error(self):
            e = self.error if self.error != None else ""
            self.error = ""
            return e

        def calculate_urls(self, url_base, total_resultados):
            """ Si esta activado el modo paginacion: retornamos una url con su data 
            con los parametros de Paginator. Si no esta activado el modo paginacion 
            devolvemos listado con (url, data), donde data contiene la cantidad total de resultados.
            Esto es asi porque la paginacion de demotores es flexible, de una peticion http podemos obtener
             todos los resultados.             
            """
            if self.paginate:                
                data = {"vars":"m,o,", "orden":"pa", "app": self.size, "page": self.page}
                return [ (url_base, data)]
            else:
                data = {"vars":"m,o,","orden":"pa", "app": total_resultados, "page": 1}
                return [(url_base, data)]

    class Auto:
        def __init__(self, nombre, descripcion, link, imagen, precio, moneda, km, combustible, garantia, detalle, fecha):
            self.nombre = nombre
            self.descripcion = descripcion
            self.link = link
            self.imagen = imagen
            self.precio = precio
            self.moneda = moneda
            self.km = km
            self.combustible = combustible
            self.garantia = garantia
            self.detalle = detalle
            self.fecha = fecha
        
        def __str__(self):
            return "Auto %s %s (Precio %s) (Km %s)" %(self.nombre, self.descripcion, self.precio, self.km) 

        def serialize(self):
            return {
                "nombre": self.nombre,
                "descripcion": self.descripcion,
                "link": self.link,
                "imagen": self.imagen,
                "precio": self.precio,
                "moneda": self.moneda,
                "km": self.km,
                "combustible": self.combustible,
                "garantia": self.garantia,
                "detalle": "",
                "fecha": self.fecha     
            }
    Auto = Auto

    def set_paginator(self, size, page):
        self.paginator.configure(size, page)
        return self.paginator        
    
    def get_autos_usados(self, marca = '', modelo = ''):
        """ Retornamos list [Auto].len <= limit coincidentes con autos usados registrados en Carone, 
            y con los parametros marca y modelo.
         """
        url = self.url_usados + marca+"-"+modelo
        return self._get_autos(url)
    
    def get_autos_0km(self, marca = '', modelo = ''):
        """ Retornamos list [Auto].len <= limit coincidentes con autos 0 km registrados en Carone, 
            y con los parametros marca y modelo. 
         """
        url = self.url_0km + marca+"-"+modelo
        return self._get_autos(url)
        
    def _get_autos(self, url):
        _, response = self.go_to(url = url, data = {})
        total = self.parse_total(response)
        urls = self.paginator.calculate_urls(url, total)
        responses = [self.go_to(url, data) for url, data in urls]
        divs = [ self.parse_divs(response[1]) for response in responses ]
        divs = [d for div in divs for d in div]
        return self.parse_autos(divs)
    
    def parse_total(self, html):
        """ Parseamos la pagina para obtener el total de resultados. Si no se encuentra
        retornamos 0
        """
        try:
            soup = BeautifulSoup(html, 'html.parser')
            h3 = soup.find('h3', attrs = {"class": "resultado"})
            result = re.match("(?P<total>\w+)", h3.get_text()).groupdict()
            return result.get('total')
        except:
            return 0        
        
    def parse_divs(self, html):
        """Recorremos todas las paginas de resultados juntando los <div> que contienen 
            datos de los autos. Verificamos que no se supere el limite solicitado.
            Retornamos un listado de objetos bs4 que corresponden con dichos <div>
        """
        soup = BeautifulSoup(html, 'html.parser')
        return  soup.select("div[class*=element]")
                    

    def parse_autos(self, divs):
        """ Scrapeamos respuesta devolviendo list [Auto]"""
        autos = []
        for div in divs:
            detalles = div.find('ul', {"class":"detalles"}).find_all('h5')
            if (div.find('div', attrs={"class":"oferta"})):
                precio_text = div.find('div', {"class":"precio"}).text
            else:
                precio_text = div.find('div', {"class":"precio"}).find("h4").text
            precio_text = precio_text.split()
            precio = precio_text.pop()
            moneda = " ".join(precio_text[:len(precio_text)])

            #saltenado el hotlinking de carone
            img_tmp = self.url_carone + div.find('img').attrs.get('src')
            img_tmp = img_tmp.replace("/mthumb.php?src=","/")
            img_tmp = re.sub(r"&w=\d+&h=\d+","",img_tmp)
            
            autos.append(
                self.Auto(
                    link = div.find("a").attrs.get('href'),
                    precio = precio,
                    moneda = moneda,
                    imagen = img_tmp, 
                    nombre = div.find('h2').text,
                    descripcion = div.find_all('h3')[1].text,
                    km = detalles[0].text,
                    combustible = detalles[1].text,
                    garantia = detalles[2].text,
                    detalle = "",
                    fecha = datetime.datetime.now().strftime("%d/%m/%Y")
                )
            )
        return autos

def main():
    from timeit import default_timer as timer

    c = Carone()
    c.set_paginator(size = 18, page = 2)
    comienzo = timer()    
    autos = c.get_autos_0km()
    autos.extend(c.get_autos_usados())
    fin = timer() - comienzo
    print "FIN ", fin, " Total", len(autos)
    for auto in autos:
        print auto

if __name__ == '__main__':
    main()