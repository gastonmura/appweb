import json
from bs4 import BeautifulSoup 
import re
from Scraper import Scraper

class PrecioScrapper(Scraper):

    url = "http://www.acara.org.ar/guiaprecios/ver_precios.php"
    Auto = None
    Precio = None

    class Precio:
    
        def __init__(self):
            self.anio = 0
            self.precio = 0
            self.moneda = ""
            
        def __init__(self, anio, moneda, precio):
            self.anio = anio
            self.moneda = moneda
            self.precio = precio
        
        def serialize(self):
            return {
                "anio": self.anio,
                "moneda": self.moneda,
                "precio": self.precio
            }
        def __str__(self):
            return "Precio (%s, %s%s)" % (self.anio, self.moneda, self.precio)
        
    class Auto:  
        
        def __init__(self):
            self.version = ""
            self.modelo = ""
            self.marca = ""
            self.precios = []
        
        def __init__(self, marca, modelo, version):
            self.marca = marca.strip()
            self.version = version.strip()
            self.modelo = modelo.strip()
            self.precios = []
        
        def add_precio(self, precio):
            self.precios.append(precio)
        
        def get_ultimo_precio(self):
            p_precio = re.compile("\d+,\d+")
            for precio in sorted(self.precios, key=lambda p: p.anio, reverse=True):
                if p_precio.match(precio.precio):
                    return precio           
            return None
       
        def serialize(self):
            return {
                "marca": self.marca,
                "modelo": self.modelo,
                "version": self.version,
                "precios": [ p.serialize() for p in self.precios]
            }

        def __str__(self):
            return "Auto (%s, %s, %s)" % (self.marca, self.modelo, self.version)

    Auto = Auto
    Precio = Precio

    def get_auto(self, marca, modelo = '0', version = '0'):
        """ Retornamos instancia de Acara Auto correspondiendo a los parametros recibidos. """
        data = {
            "marID" : marca,
            "modID" : modelo,
            "verID" : version
        }
        url, response = self.go_to(url = self.url, data = data)
        auto = self.parse_precio(url, response)
        return auto.pop() if auto else None

    def get_autos(self, marca, modelo, version):
        """ Retornamos [Acara Auto] correspondientes a los parametros recibidos. """
        data = {
            "marID" : marca,
            "modID" : modelo,
            "verID" : version
        }
        url, response = self.go_to(url = self.url, data = data)
        autos = self.parse_precio(url, response)
        return autos

    def parse_precio(self, url, html):
        """ Scrapeamos tabla de precios de Acara, devolviendo list [Auto] """
        if url != self.url:
            return  []
        soup = BeautifulSoup(html, 'html.parser')
        table = soup.find("table", attrs={"class":"texto12"})
        if table == None:
            return []
        trs = table.find_all(lambda tag: tag.name=='tr')
        anios = []
        autos = []##
        for tr in trs:
            auto = None
            if tr.attrs.get("class") and "texto12" in tr.attrs.get("class"):
                anios = [td.string for td in tr.find_all('td') if not td.string.isspace()]
            elif tr.find("td").attrs.get('class') and 'blancofondoceleste' in tr.find("td").attrs.get('class'):
                marca = tr.find("td").string
            elif tr.attrs.get('class') and  'blancofondovioleta' in tr.attrs.get('class'):
                modelo = tr.find("td").string
            else:
                tds = tr.find_all("td")
                version = tds[0].string
                auto = self.Auto(marca, modelo, version)        
                moneda = tds[1].string.strip()
                for i in range(len(tds[2:])-1):
                    auto.add_precio (self.Precio( anio = anios[i], moneda = moneda, precio = tds[i+2].string))            
                autos.append(auto)
        return autos