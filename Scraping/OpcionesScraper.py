# -*- coding: utf-8 -*-
from Scraper import Scraper
from bs4 import BeautifulSoup 
import json

class OpcionesScraper(Scraper):

    name = "opciones"
    urls = [
        "http://www.acara.org.ar/guiaprecios/precios.php",
        "http://www.acara.org.ar/javascript/rsss.php"
    ]
    scrapeado = None
    
    def get_opciones(self):
        """Retornamos dict con las opciones de los dropdown de acara:
            marca -> modelos -> versiones
        """
        _, response = self.go_to(self.urls[0])
        marcas = self.parse_marcas(response)
        for marca in marcas:
            data = {"marID": marca['codigo']}
            _, response = self.go_to(self.urls[1], data)
            modelos = self.parse_modelos(response)
            marca['modelos'] = modelos
            for modelo in marca['modelos']:
                data = {"marID": marca["codigo"], "modID": modelo["codigo"]}
                _, response = self.go_to(self.urls[1], data)
                versiones = self.parse_versiones(response)
                modelo['versiones'] = versiones
        datos = {"marcas": marcas}
        self.scrapeado = datos
        return self.scrapeado
    
    def parse_marcas(self, html):
        """ Scrapeamos dropdown de marcas de acara devolviendo list [{codigo, label}]"""
        soup = BeautifulSoup(html, 'html.parser')
        select = soup.find("select", attrs={"name":"marID"})
        result = []
        for option in select.find_all("option"):
            result.append({
                "codigo": option["value"].encode("UTF-8"),
                "label": option.string.encode("UTF-8")
            })
        return result

    def parse_modelos(self, html):
        """ Scrapeamos dropdown de modelos de acara devolviendo list [{codigo, label}]"""
        result = []
        for opcion in html.split("\\n"):
            try:
                codigo, label = opcion.split("\\t")
                result.append({"codigo":codigo, "label":label})
            except:
                continue            
        return result

    def parse_versiones(self, html):
        """ Scrapeamos dropdown de versiones de acara devolviendo list [{codigo, label}]"""
        result = []
        for option in html.split("\\n"):
            try:
                codigo, label = option.split("\\t")
                result.append({ "codigo": codigo, "label": label })
            except:
                continue
        return result
    
def main():
    o = OpcionesScraper()
    print o.get_opciones()
    
if __name__ == '__main__':
    main()