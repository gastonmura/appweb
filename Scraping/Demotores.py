# -*- coding: utf-8 -*-
from Scraper import Scraper
from bs4 import BeautifulSoup 
import re
class Demotores(Scraper):
    
    url_precios ='https://beta.demotores.com.ar/en-venta/autos-camionetas-y-4x4-tipo'
    url_precios = 'https://beta.demotores.com.ar/Search/Bind'
    url_precios = 'https://demotores.com.ar/VehicleSearchForm/Search/Bind'
    url_demotores = "https://beta.demotores.com.ar"

    class Paginator():
        """ Clase que encapsula comportamiento de paginacion de la pagina Demotores.
        Se envian parametros en la url con method  GET:
          Ejemplos: 
          s = 0 y l = 60: estamos pidiendo los primeros 60 resultados.
          s = 120 y l = 60: estamos pidiendo 60 resultados a partir del resultados 120
        """        
        SIZE = [20, 40 ,60] # valores permitidos de paginacion
        
        def __init__(self, paginate = False):
            """ Definimos valores por defecto: primera pagina con mayor cantidad de resultados 
             posibles.
             Parametros:
             Paginate : booleano que representa si esta activada la paginacion o no. Si no esta activada
              Se devolveran todos los resultados encontrados.
             """
            self.offset = 0
            self.size = self.SIZE[-1]
            self.paginate = paginate
        
        def configure(self, size, offset):
            if size == -1:
                size = self.SIZE[-1]
                print "configurado paginador por defecto ", size
            self.size = size
            self.offset = offset

        def is_valid(self):
            """ Los valores de paginacion deben ser: valores enteros, y size
             debe ser igual a los valores permitidos por demotores.
            """
            valido = True
            try:
                self.offset = int(self.offset)
                self.size = int(self.size)
                if not self.size in self.SIZE:
                    self.error = "size debe ser " + str(self.SIZE)
                    valido = False
            except ValueError:
                self.error  = "size y offset deben ser valores enteros."
                valido = False
            return valido
        
        def get_error(self):
            """ Retornamos string error si existe, sino retornamos cadena vacia."""
            e = self.error if self.error != None else ""
            self.error = ""
            return e
        
        def define_urls(self, url_base, total_resultados):
            """ En base a los parametros de paginacion, determina las url a vicitar 
             Si no esta activado el modo paginacion, se devolveran todas las urls para obtener 
              todos los resultados. Caso contrario se devuvleve [url] la url que corresponde con la paginacion.
              Retornamos [] si no existen url a vicitar: pagina pedida no tiene autos.
            """
            urls = []
            url_base = url_base + "?l=%d&s=%d"
            if self.paginate:
                if self.offset <= total_resultados: 
                    url = url_base %(self.size, self.offset )
                    urls.append( url )
            else:
                offset = self.offset
                while offset < total_resultados:
                    urls.append(url_base % (self.size, offset))
                    offset = offset + self.size
            return urls

    class Auto:
        
        def __init__(self, nombre, km, precio, moneda, link, detalle, fecha):
            self.nombre = nombre 
            self.km = km
            self.precio = precio
            self.imagenes = []
            self.link = link
            self.moneda = moneda
            self.detalle = detalle
            self.fecha = fecha
            
        def add_imagen(self, imagen):
            self.imagenes.append(imagen)
        
        def serialize(self):
            return {
                "nombre": self.nombre,
                "km": self.km,
                "precio": self.precio,
                "imagenes": self.imagenes,
                "moneda": self.moneda,
                "link": self.link,
                "detalle": self.detalle,
                "fecha": self.fecha                
            }
        
        def __str__(self):
            return "Auto (%s, Precio (%s, %s))" % (self.nombre, self.moneda, self.precio)

    Auto = Auto
    Paginator = Paginator

    def __init__(self):
        Scraper.__init__(self)
        self.paginator = self.Paginator(paginate = False)
    
    def get_autos(self, marca="", modelo="", query=""):
        """ Retornamos list [Auto] que coincidan con los parametros.
        Si no se configuro el Paginador, se devolveran  devolver todos los autos encontrados.        
         """
        data = {
            "name": "search",
            "SearchFormData.Keywords": query,
            "SearchFormData.Make": marca,
            "SearchFormData.Model": modelo
        }

        url, response = self.go_to(url = self.url_precios, data = data)
        total = self.parse_total(response)
        urls = self.paginator.define_urls(url, total)
        
        responses = [ self.go_to(url) for url in urls ]
        
        divs = [ self.parse_divs(response[1]) for response in responses ]
        divs = [d for div in divs for d in div]

        return self.parse_autos(divs)
    
    def set_paginator(self, size, offset):
        """" Iniciamos Paginador. 
         Parametros:
            sice:  cantidad de autos que se esperan recibir. Si el valor es -1 se definira
            el valor por defecto del paginator.
            offset: retornan resultados a partir de offset.
        """
        self.paginator.configure(size, offset)
        self.paginator.paginate = True
        return  self.paginator

    def parse_total(self, html):
        """ Usamos btf4 para parsear el html recibido por parametro, buscando el numero 
        total de resultados encontrados. Si no se logra scrapear el numero se retorna 0.
        """
        soup = BeautifulSoup(html, 'html.parser')
        inputs = soup.find_all('input', attrs = {"class": "cs-inav__filter-item-checkbox", "checked":"checked"} )
        try:
            total_resultados = re.compile('\d+').findall(inputs.pop().parent.find("span").get_text()).pop()
        except:
            total_resultados = 0
        return int(total_resultados)
        

    def parse_divs(self,html):
        """ Usamos btf4 para parsear el html recibido por parametros, buscando los divs
            que contienen datos de autos de Demotores.
            Retornamos un listado de objetos bs4 que corresponden con dichos <div>
        """
        soup = BeautifulSoup(html, 'html.parser')
        divs = []

        results = soup.find_all(attrs = {"class": 'listing-item row '})
        results.extend(soup.find_all(attrs = {"class": 'listing-item row margin-right'}))
        divs.extend(results)        
        return divs

    def parse_autos(self, divs):
        """ Scrapeamos respuesta devolviendo list [Auto]"""
        autos = []
        for div in divs:
            precio_text = div.find('h4', attrs={"class":"price"}).get_text().strip()
            precio_text = precio_text.split()
            precio = precio_text.pop()
            moneda = " ".join(precio_text[:len(precio_text)])
            
            auto = self.Auto(
                    nombre = div.find('h4', attrs={"class":"listing-item__title"}).get_text().strip(),
                    km = div.find("li", attrs= {"title":"Kilómetros"}).get_text().strip().split()[1],
                    precio = precio,
                    moneda = moneda,
                    link = self.url_demotores+div.find('a').attrs.get('href'),
                    detalle = "",
                    fecha = ""
                )
            for imagen in div.find_all('img'):
                if imagen.attrs.get('data-lazy'):
                    auto.add_imagen(imagen.attrs.get('data-lazy').split('?')[0])
            autos.append(auto)
        return autos


def main():
    from timeit import default_timer as timer
    demotores = Demotores()
    demotores.set_paginator(size = 60, offset = 0)
    comienzo = timer()
    autos = demotores.get_autos("chevrolet")
    fin = timer() - comienzo
    print "FIN ", fin, " Total", len(autos)
    for auto in [a.serialize() for a in autos]:
        print auto


if __name__ == '__main__':
    main()